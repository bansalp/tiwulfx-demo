package com.panemu.tiwulfx.demo.pojo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Person.class)
public abstract class Person_ {

	public static volatile SingularAttribute<Person, Insurance> insurance;
	public static volatile SingularAttribute<Person, Integer> id;
	public static volatile SingularAttribute<Person, Double> weight;
	public static volatile SingularAttribute<Person, String> email;
	public static volatile SingularAttribute<Person, String> name;
	public static volatile SingularAttribute<Person, Character> gender;
	public static volatile SingularAttribute<Person, Integer> visit;
	public static volatile SingularAttribute<Person, Date> birthDate;
	public static volatile SingularAttribute<Person, Boolean> alive;
	public static volatile SingularAttribute<Person, String> birthPlace;
	public static volatile SingularAttribute<Person, Integer> version;

}

