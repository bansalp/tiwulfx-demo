/*
 * License GNU LGPL
 * Copyright (C) 2013 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.demo.mainmenu;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.image.ImageView;

/**
 *
 * @author Amrullah <amrullah@panemu.com>
 */
public class MainMenuCategory extends MainMenuItem {
    private List<MainMenuItem> lstMenuItem = new ArrayList<>();
    public MainMenuCategory(ImageView icon, String label) {
        super(icon, label, null);
    }
    
    public void addMainMenuItem(MainMenuItem ... menuItem) {
        for (MainMenuItem item : menuItem) {
            lstMenuItem.add(item);
        }
    }

    public List<MainMenuItem> getMenuItems() {
        return lstMenuItem;
    }
}
