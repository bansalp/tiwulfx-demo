/*
 * License GNU LGPL
 * Copyright (C) 2013 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.demo.mainmenu;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Popup;
import javafx.stage.WindowEvent;

/**
 *
 * @author Amrullah <amrullah@panemu.com>
 */
public class MainMenu extends VBox {

    private List<MainMenuItem> lstMenuItem = new ArrayList<>();
    private BooleanProperty expanded = new SimpleBooleanProperty(true);
    private BooleanProperty armed = new SimpleBooleanProperty(false);
    private ScrollPane expScrollPane = new ScrollPane();
    private StackPane expStackPane = new StackPane();
    private VBox expMenuContainer = new VBox();
    private Button btnExpand = new Button();
    private Popup popup;
    private VBox infoBox;
    private Label infoName;
    private Map<TitledPane, VBox> map = new HashMap<>();
    private ImageView imgCollapse = new ImageView(new Image(MainMenu.class.getResourceAsStream("/images/left.png")));
    private ImageView imgExpand = new ImageView(new Image(MainMenu.class.getResourceAsStream("/images/right.png")));
    private MainMenuFacade facade;

    public MainMenu() {
        super();
//        btnExpand.setGraphic(new ImageView(new Image(MainMenu.class.getResourceAsStream("/image/left.png"))));
        btnExpand.graphicProperty().bind(Bindings.when(expanded).then(imgCollapse).otherwise(imgExpand));
        expMenuContainer.setAlignment(Pos.TOP_CENTER);
        btnExpand.getStyleClass().add("collapse-button");
        StackPane.setMargin(btnExpand, new Insets(0, 2, 0, 0));
        StackPane.setAlignment(btnExpand, Pos.TOP_RIGHT);

        expStackPane.getChildren().addAll(expMenuContainer, btnExpand);
        VBox.setVgrow(expStackPane, Priority.ALWAYS);
        expScrollPane.setContent(expStackPane);
        expScrollPane.setFitToWidth(true);
        expScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        expStackPane.getStyleClass().add("menu-container");
//        this.setPrefWidth(200);
        this.setMinWidth(50);
        this.getChildren().add(expStackPane);
        btnExpand.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                expanded.set(!expanded.get());
            }
        });
        expanded.addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                for (Node node : expMenuContainer.getChildren()) {
                    if (node instanceof TitledPane) {
                        TitledPane pane = (TitledPane) node;
                        pane.setContentDisplay(newValue ? ContentDisplay.LEFT : ContentDisplay.GRAPHIC_ONLY);
                        if (newValue) {
                            pane.setContent(map.get(pane));
                        } else {
                            pane.setContent(null);
                        }
                        pane.setCollapsible(newValue);
                    } else if (node instanceof Button) {
                        Button btn = (Button) node;
                        btn.setContentDisplay(newValue ? ContentDisplay.LEFT : ContentDisplay.GRAPHIC_ONLY);
                    }
                }
                if (newValue) {
                    expStackPane.getChildren().add(btnExpand);

                } else {
                    expMenuContainer.getChildren().add(0, btnExpand);
                }
            }
        });

    }

    public MainMenuFacade getFacade() {
        return facade;
    }

    public void setFacade(MainMenuFacade facade) {
        this.facade = facade;
    }
    
    private Popup getPopup() {
        if (popup == null) {
            infoBox = new VBox();
            infoBox.setId("main-menu-popup");
            infoBox.setFillWidth(true);
            infoBox.setMinWidth(USE_PREF_SIZE);
//            infoBox.setPrefWidth(350);
            infoName = new Label();
            infoName.setId("main-menu-popup-header");
            infoName.setMinHeight(USE_PREF_SIZE);
            infoName.setPrefHeight(28);
            infoBox.getChildren().addAll(infoName);

            popup = new Popup();
            popup.getContent().setAll(infoBox);
            popup.setAutoHide(true);
            popup.setConsumeAutoHidingEvents(false);
            popup.setOnHiding(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    TitledPane pane = (TitledPane) popup.getOwnerNode();
                    VBox container = map.get(pane);
                    List<Node> lstNode = new ArrayList<>(infoBox.getChildren());
                    for (Node node : lstNode) {
                        if (node != infoName && !(node instanceof Label)) {
                            container.getChildren().add(node);
                        }
                    }
                }
            });

            popup.setOnAutoHide(new EventHandler<Event>() {
                @Override
                public void handle(Event event) {
                    armed.set(false);
                }
            });

        }
        return popup;
    }

    private void prepareContent(TitledPane pane) {
        VBox container = map.get(pane);
        infoBox.getChildren().addAll(container.getChildren());
    }

    public void addMenuItem(MainMenuItem... menuItem) {
        for (MainMenuItem cat : menuItem) {
            lstMenuItem.add(cat);
            if (cat instanceof MainMenuCategory) {
                VBox content = new VBox();
                content.setMinWidth(0);
                MainMenuCategory category = (MainMenuCategory) cat;
                for (MainMenuItem mainMenuItem : category.getMenuItems()) {
                    Button btn = createExpandedMenuItemControl(mainMenuItem);
                    btn.getStyleClass().add("main-menu-button");
                    content.getChildren().add(btn);
                }
                final TitledPane pane = new TitledPane();
                map.put(pane, content);
                pane.setText(category.getLabel());
                pane.setGraphic(category.getIcon());
                pane.setUserData(cat);
                pane.setContent(content);
                pane.setExpanded(false);

                pane.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if (!expanded.get()) {
                            if (popup == null || !popup.isShowing()) {
                                TitledPane pane = (TitledPane) event.getSource();
                                armed.set(true);
                                showPopupMenu(pane);
                            } else {
                                popup.hide();
                                armed.set(false);
                            }
                        }
                    }
                });
                pane.setOnMouseEntered(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if (armed.get()) {
                            if (popup.isShowing()) {
                                popup.hide();
                            }
                            TitledPane pane = (TitledPane) event.getSource();
                            showPopupMenu(pane);
                        }
                    }
                });
                expMenuContainer.getChildren().add(pane);
            } else {
                Button btn = createExpandedMenuItemControl(cat);
                btn.getStyleClass().add("main-menu-button-item");
                btn.setOnMouseEntered(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        if (popup != null && popup.isShowing()) {
                            popup.hide();
                        }
                    }
                });
                expMenuContainer.getChildren().add(btn);
            }
        }
    }

    private void showPopupMenu(TitledPane pane) {
        Scene scene = pane.getScene();
        final Point2D windowCoord = new Point2D(scene.getWindow().getX(), scene.getWindow().getY());
        final Point2D sceneCoord = new Point2D(scene.getX(), scene.getY());
        final Point2D nodeCoord = pane.localToScene(0.0, 0.0);
        final double clickX = Math.round(windowCoord.getX() + sceneCoord.getX() + nodeCoord.getX());
        final double clickY = Math.round(windowCoord.getY() + sceneCoord.getY() + nodeCoord.getY());
        Popup p = getPopup();
        infoName.setText(pane.getText());
        prepareContent(pane);
        p.show(pane, clickX + pane.getWidth(), clickY);
    }

    private Button createExpandedMenuItemControl(MainMenuItem mainMenuItem) {
        final Button btnMenuItem = new Button(mainMenuItem.getLabel(), mainMenuItem.getIcon());
        btnMenuItem.setUserData(mainMenuItem);
        btnMenuItem.setOnAction(eventHandler);
        btnMenuItem.setMaxWidth(Double.MAX_VALUE);
        btnMenuItem.setAlignment(Pos.CENTER_LEFT);
        return btnMenuItem;
    }
    private EventHandler<ActionEvent> eventHandler = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            try {
                if (popup != null && popup.isShowing()) {
                    popup.hide();
                    armed.set(false);
                }
                Button btn = (Button) event.getSource();
                MainMenuItem mainMenuItem = (MainMenuItem) btn.getUserData();
                Method method = facade.getClass().getMethod(mainMenuItem.getActionName(), null);
                method.invoke(facade, null);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(MainMenu.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(MainMenu.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(MainMenu.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchMethodException ex) {
                Logger.getLogger(MainMenu.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SecurityException ex) {
                Logger.getLogger(MainMenu.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    };
}
