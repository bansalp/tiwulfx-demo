/*
 * License GNU LGPL
 * Copyright (C) 2012 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.demo;

//import com.javafx.experiments.scenicview.ScenicView;
import com.panemu.tiwulfx.common.Version;
import com.panemu.tiwulfx.demo.misc.DataGenerator;
import com.panemu.tiwulfx.demo.ui.FrmMain;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Amrullah <amrullah@panemu.com>
 */
public class TiwulfxDemo extends Application {

    public static EntityManagerFactory factory = Persistence.createEntityManagerFactory("tiwulfx-demoPU");
    public static Stage mainStage;

    @Override
    public void start(Stage primaryStage) {
//        generateData();

        Scene scene = new Scene(new FrmMain());
        String version = Version.class.getPackage().getImplementationTitle() + " " + Version.class.getPackage().getImplementationVersion();
        primaryStage.setTitle("TiwulFX Demo [Using " + version + "]");
        scene.getStylesheets().add("tiwulfx.css");
        scene.getStylesheets().add("tiwulfx-demo.css");
        primaryStage.setScene(scene);
        primaryStage.show();
        mainStage = primaryStage;
//        ScenicView.show(mainStage.getScene());
    }
    
    private void generateData() {
        DataGenerator.createWithTestData(100);
    }


    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
