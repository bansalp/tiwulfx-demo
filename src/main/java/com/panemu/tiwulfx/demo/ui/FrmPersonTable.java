/*
 * License GNU LGPL
 * Copyright (C) 2012 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.demo.ui;

import com.panemu.tiwulfx.common.TableData;
import com.panemu.tiwulfx.common.TiwulFXUtil;
import com.panemu.tiwulfx.control.LookupFieldController;
import com.panemu.tiwulfx.demo.DaoBase;
import com.panemu.tiwulfx.demo.TiwulfxDemo;
import com.panemu.tiwulfx.demo.misc.DataGenerator;
import com.panemu.tiwulfx.demo.misc.ProgressBarColumn;
import com.panemu.tiwulfx.demo.pojo.Insurance;
import com.panemu.tiwulfx.demo.pojo.Insurance_;
import com.panemu.tiwulfx.demo.pojo.Person;
import com.panemu.tiwulfx.demo.pojo.Person_;
import com.panemu.tiwulfx.dialog.MessageDialogBuilder;
import com.panemu.tiwulfx.table.CheckBoxColumn;
import com.panemu.tiwulfx.table.ComboBoxColumn;
import com.panemu.tiwulfx.table.DateColumn;
import com.panemu.tiwulfx.table.LookupColumn;
import com.panemu.tiwulfx.table.NumberColumn;
import com.panemu.tiwulfx.table.TableControl;
import com.panemu.tiwulfx.table.TableController;
import com.panemu.tiwulfx.table.TextColumn;
import com.panemu.tiwulfx.table.TickColumn;
import java.io.IOException;
import java.util.List;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.layout.VBox;

/**
 *
 * @author Amrullah <amrullah@panemu.com>
 */
public class FrmPersonTable extends VBox {

    @FXML
    protected TableControl<Person> tblPerson;
    private DaoBase<Insurance> daoInsurance = new DaoBase<>(Insurance.class);

    public FrmPersonTable(TableController<Person> controller) {
        FXMLLoader fxmlLoader = new FXMLLoader(FrmPersonTable.class.getResource("FrmPersonTable.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        this.controller = controller;
        init(true);
        tblPerson.reloadFirstPage();
    }

    protected void init(boolean showTickColumn) {
        tblPerson.setRecordClass(Person.class);
        tblPerson.setController(controller);
        tblPerson.setMaxRecord(50);
        TiwulfxDemo.factory.createEntityManager();
        TextColumn<Person> clmName = new TextColumn<>(Person_.name.getName(), 150);
        clmName.setEditable(true);
        TextColumn<Person> clmEmail = new TextColumn<>(Person_.email.getName(), 250);
        DateColumn<Person> clmBirthDate = new DateColumn<>(Person_.birthDate.getName());
        clmBirthDate.setRequired(false);
        ComboBoxColumn<Person, String> clmBirthPlace = new ComboBoxColumn<>(Person_.birthPlace.getName());
        for (String location : DataGenerator.birthPlaces) {
            clmBirthPlace.addItem(location, location);
        }

        ComboBoxColumn<Person, Character> clmGender = new ComboBoxColumn<>(Person_.gender.getName());
        clmGender.addItem("Male", 'm');
        clmGender.addItem("Female", 'f');
        clmGender.setRequired(true);

        CheckBoxColumn<Person> clmAlive = new CheckBoxColumn<>(Person_.alive.getName());
        clmAlive.setRequired(false);
        clmAlive.setLabel(TiwulFXUtil.getLiteral("alive.true"), TiwulFXUtil.getLiteral("alive.false"), TiwulFXUtil.getLiteral("alive.null"));

        NumberColumn<Person, Integer> clmInsuranceId = new NumberColumn<>(Person_.insurance.getName() + "." + Insurance_.id.getName(), Integer.class);
        clmInsuranceId.setEditable(false);

        LookupColumn<Person, Insurance> clmInsurance = new LookupColumn<>(Person_.insurance.getName(), Insurance_.code.getName(), 75);
        clmInsurance.setLookupController(insuranceLookupController);
        clmInsurance.setText(TiwulFXUtil.getLiteral("insurance.code"));

        TextColumn<Person> clmInsuranceName = new TextColumn<>(Person_.insurance.getName() + "." + Insurance_.name.getName(), 200);
        clmInsuranceName.setEditable(false);
        TableColumn clmInsuranceDesc = new TableColumn("Description");
        clmInsuranceDesc.getColumns().addAll(clmInsurance, clmInsuranceName);

        TableColumn clmInsuranceHeader = new TableColumn("Insurance");
        clmInsuranceHeader.getColumns().addAll(clmInsuranceId, clmInsuranceDesc);

        NumberColumn<Person, Integer> clmVisit = new NumberColumn<>(Person_.visit.getName(), Integer.class, 75);

        /**
         * Custom column. Not included in TiwulFX library
         */
        ProgressBarColumn<Person, Integer> clmProgress = new ProgressBarColumn<>(Person_.visit.getName());
        clmProgress.setEditable(false);
        clmProgress.setMax(5000);

        NumberColumn<Person, Double> clmWeight = new NumberColumn<>(Person_.weight.getName(), Double.class, 75);

        NumberColumn<Person, Integer> clmVersion = new NumberColumn<>(Person_.version.getName(), Integer.class, 50);
        clmVersion.setEditable(false);


        if (showTickColumn) {
            final TickColumn<Person> clmTick = new TickColumn<>();
            clmTick.setDefaultTicked(true);
            tblPerson.addColumn(clmTick);
            Button btnGetTicked = new Button("Get Ticked Records");
            btnGetTicked.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    MessageDialogBuilder.info().title("Ticked Records")
                            .message(clmTick.getTickedRecords().size() + " ticked\n" + clmTick.getUntickedRecords().size() + " unticked")
                            .show(getScene().getWindow());
                }
            });
            tblPerson.addButton(btnGetTicked);
        }
        tblPerson.addColumn(clmName, clmEmail, clmBirthPlace, clmBirthDate,
                clmGender, clmAlive,
                clmInsuranceHeader,
                clmVisit, clmProgress, clmWeight, clmVersion);
        tblPerson.setAgileEditing(true);
    }
    private TableController<Person> controller;
    private LookupFieldController<Insurance> insuranceLookupController = new LookupFieldController<Insurance>(Insurance.class) {
        @Override
        public String[] getColumns() {
            return new String[]{
                        Insurance_.id.getName(),
                        Insurance_.code.getName(),
                        Insurance_.name.getName()
                    };
        }

        @Override
        protected String getWindowTitle() {
            return "Find Insurance";
        }

        @Override
        protected void initCallback(VBox container, TableControl<Insurance> table) {
            container.setPrefWidth(500);
            table.getTableView().getColumns().get(2).setPrefWidth(300);
        }

        @Override
        protected TableData loadData(int startIndex, List filteredColumns, List sortedColumns, List sortingTypes, int maxResult) {
            return daoInsurance.fetch(startIndex, filteredColumns, sortedColumns, sortingTypes, maxResult);
        }
    };
}
