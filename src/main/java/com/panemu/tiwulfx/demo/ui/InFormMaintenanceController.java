/*
 * License GNU LGPL
 * Copyright (C) 2012 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.demo.ui;

import com.panemu.tiwulfx.common.TableCriteria;
import com.panemu.tiwulfx.common.TableData;
import com.panemu.tiwulfx.demo.DaoBase;
import com.panemu.tiwulfx.demo.TiwulfxDemo;
import com.panemu.tiwulfx.demo.pojo.Person;
import com.panemu.tiwulfx.demo.pojo.Person_;
import com.panemu.tiwulfx.dialog.MessageDialogBuilder;
import com.panemu.tiwulfx.form.Form;
import com.panemu.tiwulfx.table.TableController;
import java.util.Arrays;
import java.util.List;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Amrullah <amrullah@panemu.com>
 */
public class InFormMaintenanceController extends TableController<Person> {

    private DaoBase<Person> daoPerson = new DaoBase<>(Person.class);
    private Stage dialogStage;// = new Stage();
    private FrmPersonMaintenance frmPersonMaintenance;

    @Override
    public TableData loadData(int startIndex, List<TableCriteria> filteredColumns, List<String> sortedColumns, List<TableColumn.SortType> sortingOrders, int maxResult) {
        TableData result = daoPerson.fetch(startIndex, filteredColumns, sortedColumns, sortingOrders, maxResult, Arrays.asList(Person_.insurance.getName()));
        return result;
    }

    @Override
    public Person preInsert(Person newRecord) {
        showFrmPersonMaintenance(newRecord, Form.Mode.INSERT);
        return null;
    }

    @Override
    public boolean canEdit(Person selectedRecod) {
        if (selectedRecod == null) {
            MessageDialogBuilder.error().message("Please select a record to edit.").show(null);
            return false;
        }
        showFrmPersonMaintenance(selectedRecod, Form.Mode.EDIT);
        return false;
    }

    @Override
    public void doubleClick(Person record) {
        showFrmPersonMaintenance(record, Form.Mode.READ);
    }
    
    @Override
    public void delete(List<Person> records) {
        daoPerson.delete(records);
    }

    private void showFrmPersonMaintenance(Person person, Form.Mode mode) {
        if (dialogStage == null) {
            frmPersonMaintenance = new FrmPersonMaintenance();
            /**
             * cannot instantiate dialogStage when instantiating this class
             * because it's done in non-FX thread. It turns that instantiating
             * stage should be in FX thread..
             */
            dialogStage = new Stage();
            dialogStage.initOwner(TiwulfxDemo.mainStage);
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.setScene(new Scene(frmPersonMaintenance));
            dialogStage.getScene().getStylesheets().add("tiwulfx.css");
        }
        frmPersonMaintenance.setPerson(person);
        frmPersonMaintenance.setMode(mode);
        dialogStage.show();
    }
}
