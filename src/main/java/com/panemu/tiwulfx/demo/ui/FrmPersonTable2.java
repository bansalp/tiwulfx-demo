/*
 * License GNU LGPL
 * Copyright (C) 2012 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.demo.ui;

import com.panemu.tiwulfx.demo.pojo.Person;
import com.panemu.tiwulfx.dialog.MessageDialog;
import com.panemu.tiwulfx.dialog.MessageDialog.Answer;
import com.panemu.tiwulfx.dialog.MessageDialogBuilder;
import com.panemu.tiwulfx.table.TableControl;
import com.panemu.tiwulfx.table.TableController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBuilder;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Amrullah <amrullah@panemu.com>
 */
public class FrmPersonTable2 extends FrmPersonTable {

    public FrmPersonTable2(TableController<Person> controller) {
        super(controller);
    }

    @Override
    protected void init(boolean addTickColumn) {
        super.init(false);
        
        // Add button in TableControl's toolbar
        Button button = ButtonBuilder.create()
                .graphic(new ImageView(new Image(TableControl.class.getResourceAsStream("/images/chart.png"))))
                .build();
        
        button.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                Answer answer = MessageDialogBuilder.info().message("message.one")
                        .yesOkButtonText("I know")
                        .noButtonText("No, I don't")
                        .buttonType(MessageDialog.ButtonType.YES_NO)
                        .show(FrmPersonTable2.this.getScene().getWindow());
                if (answer == Answer.NO) {
                    MessageDialogBuilder.info().message("Maybe the developer hid it.")
                            .show(FrmPersonTable2.this.getScene().getWindow());
                }
            }
        });
        tblPerson.addButton(button);
        
        // Hide delete button
        tblPerson.setVisibleComponents(false, TableControl.Component.BUTTON_DELETE);
        
        // Add menu item to TableControl's context menu
        tblPerson.addContextMenuItem("Custom Menu Item", new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                //Show the MessageDialog later to give a chance for popup to close it self.
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        MessageDialogBuilder.warning().buttonType(MessageDialog.ButtonType.YES_NO_CANCEL)
                        .title("Custom Context Menu Item")
                        .message("You just clicked custom context menu item.")
//                        .yesOkButtonText("Yes Text")
                        .noButtonText("no.text")
                        .cancelButtonText("Cancel Text")
                        .show(FrmPersonTable2.this.getScene().getWindow());
                    }
                });
            }
        });
    }
 
}
