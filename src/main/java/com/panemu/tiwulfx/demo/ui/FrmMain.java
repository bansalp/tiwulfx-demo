/*
 * License GNU LGPL
 * Copyright (C) 2012 Amrullah <amrullah@panemu.com>.
 */
package com.panemu.tiwulfx.demo.ui;

import com.panemu.tiwulfx.control.NumberField;
import com.panemu.tiwulfx.demo.mainmenu.MainMenu;
import com.panemu.tiwulfx.demo.mainmenu.MainMenuCategory;
import com.panemu.tiwulfx.demo.mainmenu.MainMenuFacade;
import com.panemu.tiwulfx.demo.mainmenu.MainMenuItem;
import com.panemu.tiwulfx.demo.misc.DataGenerator;
import com.panemu.tiwulfx.dialog.MessageDialogBuilder;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Amrullah <amrullah@panemu.com>
 */
public class FrmMain extends VBox implements MainMenuFacade {

    @FXML 
    private Button btnGenerate; 
    @FXML
    private TabPane tabpane;
    @FXML 
    private NumberField<Integer> txtRecordCount;
    @FXML
    private MainMenu mainMenu;

    public FrmMain() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("FrmMain.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
//        tabInForm.setContent(new FrmPersonTable2(new InFormMaintenanceController()));
//        tabMasterDetail.setContent(new FrmMasterDetail());
        showAsTab(new FrmPersonTable(new InRowMaintenanceController()), "Edit in Row");
        txtRecordCount.setNumberType(Integer.class);
        btnGenerate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (txtRecordCount.getValue() != null && txtRecordCount.getValue() > 0) {
                    DataGenerator.createWithTestData(txtRecordCount.getValue());
                    MessageDialogBuilder.info().message("message.record.has.been.generated", txtRecordCount.getValue()).show(FrmMain.this.getScene().getWindow());
                    txtRecordCount.setValue(null);
                }
            }
        });
        mainMenu.setFacade(this);
        MainMenuCategory mmCat = new MainMenuCategory(new ImageView(new Image(FrmMain.class.getResourceAsStream("/images/m1.png"))), "Editing");
        MainMenuItem mmItem = new MainMenuItem(new ImageView(new Image(FrmMain.class.getResourceAsStream("/images/item1.png"))), "Edit in Row" , "showFrmPersonTable");
        mmCat.addMainMenuItem(mmItem);
        mmItem = new MainMenuItem(new ImageView(new Image(FrmMain.class.getResourceAsStream("/images/item2.png"))), "Edit in Form" , "showFrmPersonTable2");
        mmCat.addMainMenuItem(mmItem);
        mainMenu.addMenuItem(mmCat);
        mmItem = new MainMenuItem(new ImageView(new Image(FrmMain.class.getResourceAsStream("/images/m2.png"))), "Master Detail" , "showFrmMasterDetail");
        mainMenu.addMenuItem(mmItem);
    }
    
    public void showAsTab(Pane frm, String label) {
        final Tab tab = new Tab(label);
        tab.setClosable(true);
        tab.setContent(frm);
        tabpane.getTabs().add(tab);
        tabpane.getSelectionModel().select(tab);
        
        /**
         * Workaround for TabPane memory leak
         */
        tab.setOnClosed(new EventHandler<Event>() {

            @Override
            public void handle(Event t) {
                tab.setContent(null);
            }
        });
        
    }   
    
    public void showFrmPersonTable() {
        showAsTab(new FrmPersonTable(new InRowMaintenanceController()), "Edit in Row");
    }
    
    public void showFrmPersonTable2() {
        showAsTab(new FrmPersonTable2(new InFormMaintenanceController()), "Edit in Form");
    }
    
    public void showFrmMasterDetail() {
        showAsTab(new FrmMasterDetail(), "Master Detail");
    }
}
